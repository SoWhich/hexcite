-- ffi functions as follows in pseudo-ts notation:

-- point = {x: number, y: number}
-- axial point (x + y + z = 0)

-- sprite = {
--   file: string,
--   height: number,
--   width: number,
--   animations: {[string]: {left: number, top: number}}
-- }

-- spriteid = (opaque)

-- __single_add(x: number, y: number): void
-- adds a single point to the level

-- __single_del(x: number, y: number): void
-- removes a single point from the level

-- __add_sprite(sprite): spriteid
-- adds a sprite to memory's shared sprite cache

-- __assoc_sprite(spriteid, point): void
-- associate a sprite with a point on the map

function table.flatten(tb)
  local res = {}
  for _, child in ipairs(tb) do
    for _, member in ipairs(child) do
      table.insert(res, member)
    end
  end
  return res
end

function add_points(...)
  local t = table.pack(...)
  for _, pts in ipairs(t) do
    for _, point in ipairs(pts) do
      __single_add(point.x, point.y)
    end
  end
end

function del_points(...)
  local t = table.pack(...)
  for _, pts in ipairs(t) do
    for _, point in ipairs(pts) do
      __single_del(point.x, point.y)
    end
  end
end

function circle(center, radius)
  local tb = {{center}}
  for r=1,radius do
    table.insert(tb, ring(center, r))
  end
  return table.flatten(tb)
end


local function c(center)
  return function (offset)
    return {x = center.x + offset.x, y = center.y + offset.y}
  end
end

function ring(center, radius)
  local ring = {}
  local centered = c(center)
  for val=0,radius do
    table.insert(ring, centered {x=radius, y=-val})
    table.insert(ring, centered {x=-radius, y=val})
    table.insert(ring, centered {x=-val, y=radius,})
    table.insert(ring, centered {x=val, y=-radius,})
  end
  for val=1,(radius - 1) do
    table.insert(ring, centered {x=val, y=(radius - val)})
    table.insert(ring, centered {x=-val, y=-(radius - val)})
  end
  return ring
end

function point(pt)
  return {pt}
end

function line(orig, dest)
end

sprites = {
-- protag = __add_sprite({file = 'sprites/protag.png'})
}

