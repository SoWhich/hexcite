#include <SFML/Graphics.hpp>
#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <iterator>
#include <vector>
#include <sstream>

#include "loader.hpp"
#include "config.hpp"
#include "render_logic/Hexagon.hpp"
#include "render_logic/Level.hpp"

constexpr char name[] = "0x0000";

sf::String get_fps() {
  constexpr float count_period = 0.5;
  static sf::Clock clock;
  static float count;
  static sf::String fps;

  count++;
  auto time = clock.getElapsedTime().asSeconds();
  if (time > count_period) {
    fps = {std::to_string(count / time)};
    count = 0;
    clock.restart();
  }

  return fps;
}

int main(int argc, const char** argv) {
  sf::RenderWindow window{
      sf::VideoMode{},
      static_cast<const char*>(name),
      sf::Style::Fullscreen};

  auto config = Config::loadConfig();

  //  window.setVerticalSyncEnabled(true);
  auto grid = load_level(parse_args(argc, argv));

  const auto font = [config]() {
    auto font = sf::Font{};
    // todo, bundle fonts in assets folder or something
    font.loadFromFile(config.font_file);
    return font;
  }();

  while (window.isOpen()) {
    sf::Event event = {};
    while (window.pollEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed:
          window.close();
          break;
        case sf::Event::KeyReleased:
          using Key = sf::Keyboard::Key;
          switch (event.key.code) {
            case Key::E:
              grid.rotate(&Level::right);
              break;
            case Key::Q:
              grid.rotate(&Level::left);
              break;
            case Key::W:
              grid.activate(
                  CubePoint::closestHexagon(sf::Mouse::getPosition()));
              break;
            case Key::X:
              exit(EXIT_SUCCESS);
              break;
            default:
              break;
          }
          break;
        case sf::Event::MouseButtonReleased:
          switch (event.mouseButton.button) {
            case sf::Mouse::Button::Left:
              grid.add(CubePoint::closestHexagon(
                  sf::Vector2i{event.mouseButton.x, event.mouseButton.y}));
              break;
            case sf::Mouse::Button::Right:
              grid.remove(CubePoint::closestHexagon(
                  sf::Vector2i{event.mouseButton.x, event.mouseButton.y}));
            default:
              break;
          }
          break;
        default:
          break;
      }
    }

    auto mouseloc = sf::Mouse::getPosition();
    auto point = CubePoint::closestHexagon(mouseloc);
    auto path = grid.path(grid.getActive(), point);
    auto cursor = Hexagon{point.toCenter(), dim};
    auto point_str = std::stringstream{};
    point_str << point.x << ", " << point.y << ", " << point.z();
    auto txt = sf::Text{point_str.str(), font};
    txt.setPosition(mouseloc.x, mouseloc.y);
    txt.setColor(sf::Color::Magenta);

    cursor.setFillColor(sf::Color::Transparent);
    cursor.setOutlineColor(sf::Color::Yellow);
    const auto outlineThickness = 5.F;
    cursor.setOutlineThickness(outlineThickness);

    window.clear();
    window.draw(grid);
    for (auto cp : path) {
      auto hex = Hexagon{cp.toCenter(), dim};
      hex.setFillColor(sf::Color::Transparent);
      hex.setOutlineColor(sf::Color::Red);
      hex.setOutlineThickness(outlineThickness * 2);
      window.draw(hex);
    }

    window.draw(cursor);
    window.draw(sf::Text{get_fps(), font});
    window.draw(txt);

    window.display();
  }
}
