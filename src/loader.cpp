#include "loader.hpp"

#include "util/log.hpp"

using lua_ptr = std::unique_ptr<lua_State, void (*)(lua_State *)>;

static void level_closure(
    lua_State *lua, Level *level, lua_CFunction fun, const char *name);

static int level_add(lua_State *);
static int level_del(lua_State *);

// TODO: Runtime shenanigans, os level application storage conventions,
// sanity checking
static const char *get_preamble() { return "lua/preamble.lua"; }

const char *parse_args(int argc, const char **argv) {
  if (argc != 2) {
    fputs("Failed to provide file to load level from\n", stderr);
    exit(EXIT_FAILURE);
  }
  return argv[1];
}

static inline void lua_run_verify(lua_State *, const char *);

Level load_level(const char *luafile) {
  auto level = Level{};

  auto l = ::lua_ptr{luaL_newstate(), lua_close};
  luaopen_base(l.get());
  luaopen_table(l.get());

  level_closure(l.get(), &level, &level_add, "__single_add");
  level_closure(l.get(), &level, &level_del, "__single_del");

  lua_run_verify(l.get(), get_preamble());
  lua_run_verify(l.get(), luafile);
  return level;
}

static inline void level_closure(
    lua_State *lua, Level *level, lua_CFunction fun, const char *name) {
  lua_pushlightuserdata(lua, level);
  lua_pushcclosure(lua, fun, 1);
  lua_setglobal(lua, name);
}

static int level_add(lua_State *lua) {
  if (lua_gettop(lua) != 2) {
    return luaL_error(lua, "Called add with wrong number of arguments");
  }
  void *dt = lua_touserdata(lua, lua_upvalueindex(1));
  Level &l = *reinterpret_cast<Level *>(dt);
  int x = luaL_checknumber(lua, 1);
  int y = luaL_checknumber(lua, 2);
  l.add({x, y});
  return 0;
}

static int level_del(lua_State *lua) {
  if (lua_gettop(lua) != 2) {
    return luaL_error(lua, "Called del with wrong number of arguments");
  }
  void *dt = lua_touserdata(lua, lua_upvalueindex(1));
  Level &l = *reinterpret_cast<Level *>(dt);
  int x = luaL_checknumber(lua, 1);
  int y = luaL_checknumber(lua, 2);
  l.remove({x, y});
  return 0;
}

static inline void lua_run_verify(lua_State *l, const char *luafile) {
  int err = 0;
  if ((err = luaL_dofile(l, luafile)) != 0) {
    LOG_ERROR(
        "CODE %d in file \"%s\" with error \"%s\"\n",
        err,
        luafile,
        lua_tolstring(l, 1, nullptr));
    exit(EXIT_FAILURE);
  }
}
