#ifndef __CONFIG_HPP__
#define __CONFIG_HPP__


struct Config {
	static Config loadConfig() {
		return {"/usr/share/fonts/fira-code/FiraCode-Retina.ttf"};
	}

	char const*font_file;
};

#endif // __CONFIG_HPP__
