#include "static_str.hpp"

#include <cstdlib>
#include <cstring>

static_str from_str(const char* str, std::size_t _len) {
  const std::size_t len = _len;
  auto buf = static_str::Bufptr{static_cast<static_str::strbuf*>(std::malloc(sizeof(static_str::strbuf) + sizeof(char[len]))), std::free};
  buf->len = len;
  std::memcpy(buf->chars, str, len);
  return static_str { std::move(buf) };
}

static_str from_cstr(const char* str) {
  const std::size_t len = std::strlen(str);
  return from_str(str, len);
}
