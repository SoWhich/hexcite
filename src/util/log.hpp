#ifndef _LOGGER_HELPER_HPP_
#define _LOGGER_HELPER_HPP_

#include <printf.h>

#ifndef DEBUG
#define LOG_DEBUG(...)                                      \
  do {                                                  \
    printf(">> ~DEBUG~ %s:%d << ", __FILE__, __LINE__); \
    printf(__VA_ARGS__);                                \
    putc('\n', stdout);                                 \
  } while (0)
#else
#define LOG_DEBUG(...) \
  do {             \
  } while (0)
#endif

#define LOG_ERROR(...)                                               \
  do {                                                           \
    fprintf(stderr, ">> !ERROR! %s:%d << ", __FILE__, __LINE__); \
    fprintf(stderr, __VA_ARGS__);                                \
    fputc('\n', stderr);                                         \
  } while (0)

#endif
