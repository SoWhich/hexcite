#ifndef __STATIC_STR_H__
#define __STATIC_STR_H__

#include <cstddef>
#include <memory>

#include <absl/strings/string_view.h>

class static_str {
  struct strbuf {
    std::size_t len;
    char chars[];
  };

  using Deleter = void (*)(void *);
  using Bufptr = std::unique_ptr<strbuf, Deleter>;

  Bufptr buf;
  static_str(strbuf *buf, Deleter deleter = free) : buf {buf, deleter} {}
  static_str(Bufptr ptr) : buf {std::move(ptr)} {}
 public:

  operator absl::string_view() const {
    return absl::string_view{buf->chars, buf->len};
  }

  bool operator==(absl::string_view sv) const {
    return sv == this->operator absl::string_view();
  }

  friend static_str from_cstr(const char*);
  friend static_str from_str(const char*, std::size_t);

  template <typename H>
  friend H AbslHashValue(H h, static_str const& c) {
    return H::combine(std::move(h), c.operator absl::string_view());
  }

};

static_str from_cstr(const char*);
static_str from_str(const char*, std::size_t);

#endif  // !__STATIC_STR_H__
