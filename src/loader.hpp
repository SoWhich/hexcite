#ifndef _DIS_IS_LOADER_HPP___
#define _DIS_IS_LOADER_HPP___
#include <lua.hpp>
#include <memory>

#include "src/render_logic/Level.hpp"

const char *parse_args(int argc, const char **argv);

Level load_level(const char *luafile);

#endif
