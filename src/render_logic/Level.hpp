#ifndef _LEVEL_HPP_
#define _LEVEL_HPP_

#include <absl/container/btree_map.h>
#include <absl/container/flat_hash_set.h>

#include <SFML/Graphics.hpp>
#include <algorithm>
#include <array>
#include <cmath>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <memory>
#include <vector>

#include "Animation.hpp"
#include "AxialPoint.hpp"
#include "Hexagon.hpp"
#include "../util/static_str.hpp"

class Level : public sf::Drawable {
 private:
  struct Value;

 public:
  using Direction = CubePoint (Level::*)(const CubePoint&) const;

  Level(std::initializer_list<CubePoint> lst) {
    this->intern(from_cstr("default"));
    for (const auto& coord : lst) {
      add(coord);
    }
  }

  Level() {
    this->intern(from_cstr("default"));
  }

  void add(CubePoint c, Value v = Value()) { t.emplace(c, std::move(v)); }
  void remove(CubePoint c) { t.erase(c); }

  void activate(CubePoint c) { active = c; }

  void join(Level&& g, CubePoint at);
  Level extract(CubePoint center);
  Level extract(CubePoint center, unsigned radius);

  CubePoint right(const CubePoint& c) const {
    const auto diff = c - active;
    return CubePoint{active.x - diff.z(), active.y - diff.x};
  }

  CubePoint left(const CubePoint& c) const {
    const auto diff = c - active;
    return CubePoint{active.x - diff.y, active.y - diff.z()};
  }

  auto begin() const { return t.begin(); }
  auto end() const { return t.end(); }
  auto count() const { return t.size(); }

  void rotate(Direction direction);

  void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
  std::vector<CubePoint> path(CubePoint, CubePoint) const;

  CubePoint getActive() { return active; }

  absl::string_view intern(static_str s) {
    return *strs.insert(std::move(s)).first;
  }

 private:
  struct Value {
    CubePoint prev;
    std::unique_ptr<Sprite> sprite;
    mutable sf::Sprite last_show;
    mutable clk::time_point last_update;

    sf::Sprite update(clk::time_point now) const {
      if (!this->sprite) {
        return sf::Sprite{};
      }
      if (now - last_update >= FrameDiff) {
        last_update = now;
        last_show = this->sprite->onNext();
      }
      return this->last_show;
    }
    Value at(CubePoint p) {
      return Value{prev + p, std::move(sprite), last_show, last_update};
    }
  };

  const clk::duration rotation_duration = std::chrono::milliseconds(700);
  absl::btree_map<CubePoint, Value, CubePoint::z_based_compare> t{};
  absl::flat_hash_set<static_str> strs{};
  mutable RadialArc anim{};

  CubePoint active{0, 0};
};

#endif
