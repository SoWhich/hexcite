#include "Level.hpp"

#include "../util/log.hpp"
#include <algorithm>

#include "absl/container/flat_hash_map.h"

void Level::rotate(Direction direction) {
  anim.start = clk::now();
  anim.len = rotation_duration;
  decltype(t) repl{};
  for (auto& pr : t) {
    repl.emplace(
        (this->*direction)(pr.first),
        Value{pr.first, nullptr, pr.second.last_show, pr.second.last_update});
  }
  std::swap(t, repl);
}

void Level::draw(sf::RenderTarget& target, sf::RenderStates states) const {
  auto time = clk::now();
  for (const auto& pr : t) {
    auto hex_center =
        anim.is_complete(time)
            ? pr.first.toCenter()
            : anim.animate_for(pr.second.prev, pr.first, active, time);

    auto h = Hexagon{hex_center, dim};
    h.setFillColor(pr.first == active ? sf::Color::Cyan : sf::Color::White);
    target.draw(h, states);

    auto sprite = pr.second.update(time);
    sprite.getGlobalBounds();
  }

  if (t.find(active) == t.end()) {
    auto h = Hexagon{active.toCenter(), dim};
    h.setFillColor(sf::Color::Blue);
    target.draw(h, states);
  }
}

void Level::join(Level&& g, CubePoint at) {
  for (auto& e : g.t) {
    t.emplace(e.first + at, e.second.at(at));
  }
}

Level Level::extract(CubePoint center) {
  auto g = Level{};
  g.t[center] = std::move(t[center]);
  t.erase(center);
  return g;
}

Level Level::extract(CubePoint center, unsigned radius) {
  auto ret = Level{};
  ret.activate({0, 0});
  for (auto i = t.begin(); i != t.end(); i++) {
    auto conv = i->first - center;
    if (conv.layer() <= radius) {
      ret.t.emplace(conv, i->second.at(center));
      t.erase(i);
    }
  }
  return ret;
}

std::vector<CubePoint> Level::path(CubePoint start, CubePoint end) const {
  // yo dude the greedy algorithm is broken. counterexamples aren't hard to make
  // just make an indirect path that's faster compared to an initially direct
  // path that's circuitous

  absl::btree_map<CubePoint, CubePoint, CubePoint::z_based_compare> prev;
  std::vector<CubePoint> next;

  auto cmp = [end](auto lhs, auto right) {
    return lhs.distance(end) >= right.distance(end);
  };
  auto push_heap = [&next, cmp](auto val) mutable {
    next.push_back(val);
    std::push_heap(std::begin(next), std::end(next), cmp);
  };
  auto pop_heap = [&next, cmp]() mutable -> CubePoint {
    std::pop_heap(std::begin(next), std::end(next), cmp);
    auto last = next[next.size() - 1];
    next.pop_back();
    return last;
  };

  push_heap(start);
  prev[start] = start;

  while (!next.empty()) {
    auto current = pop_heap();
    if (current == end) {
      auto ret = std::vector<CubePoint>{};
      auto pmem = end;
      do {
        pmem = prev[pmem];
        ret.push_back(pmem);
      } while (pmem != start);

      std::reverse(std::begin(ret), std::end(ret));
      return ret;
    }

    for (auto neighbor : current.neighbors()) {
      if (!prev.contains(neighbor) && t.contains(neighbor)) {
        prev[neighbor] = current;
        push_heap(neighbor);
      }
    }
  }

  return {};
}
