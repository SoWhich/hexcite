#ifndef _SPRITE_PERIOD_HPP_
#define _SPRITE_PERIOD_HPP_
#include "Animation.hpp"

class PeriodSprite : public BasicSprite {
  int frame_count;
  int frame_distance;
  int start_frame;

  sf::Sprite onNext(int increment) override {
    if (not texture) {
      return sf::Sprite();
    }
    auto multiplier =
        ((increment + (frame_count * frame_distance) - start_frame)
         / frame_distance)
        % frame_count;
    auto current_pos = sf::IntRect{
        start_loc.left + multiplier * start_loc.width,
        start_loc.top,
        start_loc.width,
        start_loc.height};
    return sf::Sprite(*texture, current_pos);
  }
};
#endif
