#ifndef _ANIMATION_HPP_
#define _ANIMATION_HPP_

#include <chrono>

#include "AxialPoint.hpp"
#include "Hexagon.hpp"
#include <absl/container/flat_hash_map.h>
#include <absl/strings/string_view.h>


using clk = std::chrono::steady_clock;
constexpr int FrameRate = 24;
constexpr clk::duration FrameDiff = std::chrono::seconds{1} / 24;

class Sprite {
 public:
  using AnimMap =
      std::shared_ptr<absl::flat_hash_map<absl::string_view, sf::IntRect>>;
  using Texture = std::shared_ptr<sf::Texture>;

  Sprite(AnimMap anim_map, Texture texture)
      : anim_map{std::move(anim_map)}, texture{std::move(texture)} {}

  sf::Texture *baseTexture() { return texture.get(); }

  void set_current(absl::string_view s) {
    current = s;
    increment = 0;
  }

  sf::Sprite onNext() {
    const auto &rect = (*anim_map)[current];
    const auto newrect =
        sf::IntRect{rect.width * increment, rect.top, rect.height, rect.width};
    increment = increment + 1 % FrameRate;
    return sf::Sprite(*texture, newrect);
  }

 private:
  AnimMap anim_map;
  Texture texture;
  absl::string_view current = "default";
  int increment = 0;
};

struct Normalized {
  sf::Vector2f orig;
  sf::Vector2f dest;
  sf::Vector2f pivot;
  float tngl_dist{};

  static Normalized normalize(
      CubePoint orig, CubePoint dest, CubePoint pivot) {
    sf::Vector2f _orig = orig.toCenter();
    sf::Vector2f _dest = dest.toCenter();
    sf::Vector2f _pivot = pivot.toCenter();
    sf::Vector2f diff = _orig - _pivot;

    return {
        _orig,
        _dest,
        _pivot,
        std::sqrt(diff.x * diff.x + diff.y * diff.y)};
  }
};

struct RadialArc {
  static constexpr float TOTAL_ROTATION = 720;
  clk::time_point start = clk::now();
  clk::duration len = std::chrono::seconds(1);
  bool is_complete(clk::time_point t) const { return (t - start) >= len; }

  sf::Vector2f animate_for(
      CubePoint orig,
      CubePoint dest,
      CubePoint pivot,
      clk::time_point now) const;
};

#endif
