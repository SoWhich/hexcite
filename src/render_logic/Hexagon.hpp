#ifndef __HEXAGON_HPP__
#define __HEXAGON_HPP__

#include <SFML/Graphics.hpp>

#include "AxialPoint.hpp"

class Hexagon : public sf::ConvexShape {
 public:
  explicit Hexagon(sf::Vector2f pos, const Dimensions dim)
      : sf::ConvexShape(6), dim{dim} {
    // starting from the top, going around clockwise
    setPosition(pos);
    setOrigin(pos.x + dim.box_d, pos.y + dim.tngl_d + dim.box_d);
    setPoint(0, {pos.x, pos.y + 2.F * dim.tngl_d});
    setPoint(1, {pos.x + dim.box_d, pos.y + dim.tngl_d});
    setPoint(2, {pos.x + dim.box_d, pos.y - dim.tngl_d});
    setPoint(3, {pos.x, pos.y - 2.F * dim.tngl_d});
    setPoint(4, {pos.x - dim.box_d, pos.y - dim.tngl_d});
    setPoint(5, {pos.x - dim.box_d, pos.y + dim.tngl_d});
  }

  sf::FloatRect getGlobalBounds() const {
    return sf::FloatRect(
        getPoint(5).x,
        getPoint(5).y,
        getPoint(1).x - getPoint(5).x,
        getPoint(4).y - getPoint(5).y);
  }

  bool contains(sf::Vector2f point) const {
    if (not getGlobalBounds().contains(point)) return false;
    if (sf::FloatRect(
            getPoint(4).x,
            getPoint(4).y,
            2 * dim.box_d,
            2 * dim.box_d)
            .contains(point)) {
      return true;
    }
    boost::throw_exception(std::exception());
    return true;
  }

 private:
  const Dimensions dim;
};

#endif
