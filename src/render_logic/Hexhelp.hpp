#ifndef __hexhelp_hpp__
#define __hexhelp_hpp__

#include <array>

#include "AxialPoint.hpp"

namespace {  // "Header Private"

// hack to have constexpr count of number of elements in enum
constexpr auto hexdirst = __LINE__;
enum class HexDirection {
  NorthEast = 0,
  East,
  SouthEast,
  SouthWest,
  West,
  NorthWest
};
constexpr auto hexdirsz = __LINE__ - hexdirst - 3;
}  // namespace

template <typename T>
struct InDirection : public std::array<T, ::hexdirsz> {
 private:
  using hd = std::array<T, ::hexdirsz>;
  using hd::at;
  using hd::operator[];

 public:
  constexpr InDirection(hd&& lst) : hd(lst) {}
  constexpr InDirection(const hd& lst) : hd(lst) {}
  constexpr const T& at(HexDirection d) const {
    return hd::operator[](static_cast<unsigned>(d));
  }
  constexpr T& at(HexDirection d) {
    return hd::operator[](static_cast<unsigned>(d));
  }
  constexpr const T& operator[](HexDirection d) const {
    return hd::operator[](static_cast<unsigned>(d));
  }
  constexpr T& operator[](HexDirection d) {
    return hd::operator[](static_cast<unsigned>(d));
  }
};

constexpr InDirection<CubePoint> MatchingCoord =
    std::array<CubePoint, ::hexdirsz>{
        CubePoint{1, 0},
        CubePoint{1, -1},
        CubePoint{0, -1},
        CubePoint{-1, 0},
        CubePoint{-1, 1},
        CubePoint{0, 1}};

#endif
