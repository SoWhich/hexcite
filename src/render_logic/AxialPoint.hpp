#ifndef _AXIAL_POINT_HPP_
#define _AXIAL_POINT_HPP_

#include <SFML/System.hpp>
#include <boost/throw_exception.hpp>
#include <cmath>
#include <array>

// babylonian approximation
template <typename T>
constexpr T sqrt(T base, T estimate = 1) {
  if (base < 0 || estimate < 0) {
    boost::throw_exception(
        std::domain_error("Negative number given to square root function"));
  }

  T next_estimate = .5 * (estimate + base / estimate);

  if (estimate * estimate == base || estimate == next_estimate) {
    return estimate;
  }
  return sqrt(base, next_estimate);
}

struct Dimensions {
  constexpr static float BOX_EDGE_HALF = 4.F;
  constexpr static float TRIANGLE_HEIGHT = BOX_EDGE_HALF / sqrt(3.F);
  constexpr static float SCALE_H = 8;
  constexpr static float SCALE_O = 3;

  float box_d = BOX_EDGE_HALF * SCALE_H;
  float tngl_d = TRIANGLE_HEIGHT * SCALE_H;
  float offset = SCALE_O;

  float midpoint_distance() const { return 2 * (box_d + offset); }

  void scale(float s) { *this = {box_d * s, tngl_d * s, offset * s}; }
};

const sf::Vector2f orig{960, 540};
const Dimensions dim;
constexpr size_t NUM_SIDES = 6;

struct CubePoint {
  int x, y;

  constexpr inline int z() const { return (-x - y); }

  constexpr bool operator==(CubePoint const& rhs) const {
    return x == rhs.x && y == rhs.y;
  }

  constexpr bool operator!=(CubePoint const& rhs) const {
    return x != rhs.x || y != rhs.y;
  }

  constexpr CubePoint operator+(CubePoint const& rhs) const {
    return {x + rhs.x, y + rhs.y};
  }

  constexpr CubePoint operator-(CubePoint const& rhs) const {
    return {x - rhs.x, y - rhs.y};
  }

  template <typename T>
  constexpr CubePoint operator*(T rhs) const {
    return {x * rhs, y * rhs};
  }

  constexpr unsigned layer() const {
    return std::max({std::abs(x), std::abs(y), std::abs(z())});
  }

  constexpr unsigned distance(CubePoint target) const {
    return (*this - target).layer();
  }

  constexpr std::array<CubePoint, NUM_SIDES> neighbors() const {
    return {
        *this + CubePoint{-1, 1},
        *this + CubePoint{0, 1},
        *this + CubePoint{1, 0},
        *this + CubePoint{1, -1},
        *this + CubePoint{0, -1},
        *this + CubePoint{-1, 0}};
  }

  template <typename H>
  friend H AbslHashValue(H h, const CubePoint& c) {
    return H::combine(std::move(h), c.x, c.y);
  }

  sf::Vector2f toCenter() const {
    return {
        orig.x + (dim.box_d + dim.offset) * (x - y),
        orig.y
            + (dim.tngl_d * 3.F + dim.offset * 2.F) * static_cast<float>(z())};
  }

  static CubePoint closestHexagon(sf::Vector2f point) {
    auto xminusy = float(point.x - orig.x) / float(dim.box_d + dim.offset);
    auto xplusy =
        -1.F * (point.y - orig.y) / (dim.tngl_d * 3.F + dim.offset * 2.F);
    int x = round((xplusy + xminusy) / 2);
    int y = floor(xplusy - x);
    return CubePoint{x, y};
  }

  static inline CubePoint closestHexagon(sf::Vector2i point) {
    sf::Vector2f fpoint = {
        static_cast<float>(point.x),
        static_cast<float>(point.y)};
    return closestHexagon(fpoint);
  }

  struct z_based_compare {
    using result_type = bool;
    using first_argument_type = const CubePoint&;
    using second_argument_type = const CubePoint&;
    constexpr result_type operator()(
        first_argument_type a, second_argument_type b) const {
      return (a.z() < b.z())
             || ((a.z() == b.z()) && (a.x - a.y) < (b.x - b.y));
    }
  };
};
#endif
