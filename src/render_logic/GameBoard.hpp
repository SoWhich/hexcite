#ifndef _GAMEBOARD_HPP_
#define _GAMEBOARD_HPP_
#include <absl/container/btree_map.h>

#include <SFML/Graphics.hpp>
#include <algorithm>
#include <array>
#include <boost/container_hash/hash.hpp>
#include <cmath>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <vector>

#include "Animation.hpp"
#include "AxialPoint.hpp"
#include "Hexagon.hpp"
#include "Hexhelp.hpp"

class GameBoard : public sf::Drawable {
 public:
  using Direction = CubePoint (GameBoard::*)(const CubePoint&) const;
  GameBoard(std::initializer_list<CubePoint> lst) {
    for (const auto& coord : lst) {
      add(coord);
    }
  }

  GameBoard() = default;

  void add(CubePoint c) { t.emplace(c, Value{}); }
  void remove(CubePoint c) { t.erase(c); }

  void activate(CubePoint c) { active = c; }

  void join(GameBoard& g, CubePoint at);
  GameBoard extract(CubePoint center);
  GameBoard extract(CubePoint center, unsigned radius);

  CubePoint right(const CubePoint& c) const {
    const auto diff = c - active;
    return CubePoint{active.x - diff.z(), active.y - diff.x};
  }

  CubePoint left(const CubePoint& c) const {
    const auto diff = c - active;
    return CubePoint{active.x - diff.y, active.y - diff.z()};
  }

  void rotate(Direction direction);

  void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

 private:
  CubePoint active = {0, 0};
  struct Value {
    CubePoint prev;

    Value at(CubePoint p) const { return Value{prev + p}; }
  };

  const std::chrono::duration<float, RadialArc::hrc::period>
      rotation_duration = std::chrono::milliseconds(700);
  absl::btree_map<CubePoint, Value, CubePoint::z_based_compare> t{};
  mutable RadialArc anim;
};

#endif
