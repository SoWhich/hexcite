#include "Animation.hpp"

#include "../util/log.hpp"

sf::Vector2f RadialArc::animate_for(
    CubePoint orig,
    CubePoint dest,
    CubePoint pivot,
    clk::time_point now) const {
  const decltype(len) since = now < start ? now - now : now - start;
  const auto height_func = [](float layer) -> float {
    return std::pow(1.25, layer);
  };

  const auto n = Normalized::normalize(orig, dest, pivot);

  const float progress = float(since.count()) / float(len.count());
  const sf::Vector2f diff = n.dest - n.orig;
  const sf::Vector2f line_point = diff * progress + n.orig;
  return (line_point - n.pivot)
             * (1.F
                + height_func((orig - pivot).layer()) * (progress)
                      * (1 - progress))
         + n.pivot;
}
